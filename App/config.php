<?php

namespace App;

define('ORGANIZACAO_NOME', 'Processo celetivo Kcollector');
//define('BASE_URL', 'http://localhost:81/kcollector.com.br/public/'); //para redirecionar
//Verifica qual o protocolo
$protocol = $_SERVER['SERVER_PORT'] == 443 ? "https://" : "http://";
$domainName = $_SERVER['HTTP_HOST']; // pega o dominio
$domainName = rtrim($domainName, "/"); // garante que o dominio nao veio com barra no final
$domainName .= "/"; // adiciona uma barra no fim
//define('BASE_URL',$protocol.$domainName);
define('BASE_URL', 'http://localhost/kcollector/public/');
//define('BASE_URL','http://kcollector.com.br/public/');


define('APP_URL', 'C:\\xampp\\htdocs\\kcollector\\App'); // para renderizar views
//define('APP_URL', '/var/www/kcollector.com.br/App'); // para renderizar views
define('APP_NSPACE', 'App'); // para boostrap

define('DB_TYPE', 'mysql');
define('DB_HOST', 'localhost');
define('DB_PORT', '3306');
define('DB_NAME', 'kcollector');
define('DB_USER', 'root');
define('DB_PASS', '');
//define('DB_PASS', 'lJPf5Pep9h'); 


define('DEFAULT_CONTROLLER', 'Index');
define('DEFAULT_LANGUAGE', 'ptBR');
define('DEFAULT_TEMA', 'default');
define('DB_CHAR', 'utf8');
define('SECURE', false);
define('SLEEP_BRUTE_FORCE', 3);
define('QT_TENTATIVAS_LOGIN', 6);

define('DEFAULT_PAGE_TITLE', 'kcollector'); //máximo 60 caracteres
define('DEFAULT_REMETENTE_EMAIL', 'noreply');
define('DEFAULT_REMETENTE_NOME', 'kcollector');

define('DEFAULT_CEP', '');

define('EMAIL_CONTATO', 'email@email.com');
define('NOME_CONTATO', 'kcollector');
define('ASSUNTO_CONTATO', 'Contato do site '.DEFAULT_PAGE_TITLE);

define('DIR_XML', 'C:\\xampp\\htdocs\\kcollector\\Files\\xml\\');
//define ('DIR_XML','/var/www/kcollector.com.br/Files/xml/');
define('NUM_ITENS_GRID', 25);
define('NUM_ITENS_SHOW', 50);

define('TAMANHO_MAXIMO', (2 * 1024 * 1024));

define('ANO', 2018);
define('DEFAULT_PAGE_DESCRIPTION','Descrição da página...');
define('DEFAULT_PAGE_KEYWORDS','Palavra 01, Palavra 02...');
