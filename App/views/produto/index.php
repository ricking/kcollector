<div class="row" ng-controller="produtoCtrl">
    <div class="col-md-6">
        <table class="table table-hover" id="table-produto">
            <thead>
                <tr>
                    <th class="text-center" width="30">ID</th>
                    <th>Descrição</th>
                    <th>Categoria</th>
                    <th class="text-center" width="30">Valor</th>
                    <th class="text-center" width="30">Estoque</th>
                    <th class="text-center">
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-novo">
                            Novo
                        </button>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="produto in produtos">
                    <td class="text-center"> {{produto.id_produto}} </td>
                    <td> {{produto.dc_produto}} </td>
                    <td> {{produto.no_categoria}} </td>
                    <td class="text-center"> {{produto.nm_valor}} </td>
                    <td class="text-center"> {{produto.nm_estoque}} </td>
                    <td class="text-center"> 
                        <div class="btn-group">
                            <button type="button" class="btn btn-info btn-sm" ng-click="editar(produto)">Editar</button>
                            <button type="button" class="btn btn-danger btn-xs" ng-click="excluir(produto)">Excluir</button>
                        </div>
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
    <!-- Modais de Produto -->
    <!-- Novo cadastro -->
    <div id="modal-novo" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Cadastro de Produto</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form id="form-produto">
                        <div ngIf="produto.id_produto">
                            <input type="hidden" ng-model="produto.id_produto">
                        </div>
                        <div class="form-group">
                            <label for="nome">Descrição:</label>
                            <input type="text" class="form-control" ng-model="produto.dc_produto">
                        </div>
                        <div class="form-group">
                            <label for="email">Categoria:</label>
                            <select class="form-control" ng-model="produto.id_categoria">
                                <option ng-repeat="categoria in categorias" value="{{categoria.id_categoria}}">
                                    {{categoria.no_categoria}}
                                </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="nome">Valor:</label>
                            <input type="text" id="valor" class="form-control" ng-model="produto.nm_valor">
                        </div>
                        <div class="form-group">
                            <label for="nome">Estoque:</label>
                            <input type="text" class="form-control" ng-model="produto.nm_estoque">
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <button type="button" ng-click="cadastro(produto)" class="btn btn-success">Cadastrar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
    <!-- Modal Alert Mensagem -->
    <div id="modal-alert" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" ng-click="load()">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="alert {{classe}} alert-dismissible">
                        <strong>{{alerta}}</strong> {{mensagem}}.
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- ./Fim do controller -->