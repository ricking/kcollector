<div class="row" ng-controller="categoriaCtrl">
    <div class="col-md-6">
        <table class="table table-hover" id="table-categoria">
            <thead>
                <tr>
                    <th class="text-center" width="30">ID</th>
                    <th>Nome</th>
                    <th class="text-center">
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-novo">
                            Novo
                        </button>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="categoria in categorias">
                    <td class="text-center"> {{categoria.id_categoria}} </td>
                    <td> {{categoria.no_categoria}} </td>
                    <td class="text-center"> 
                        <div class="btn-group">
                            <button type="button" class="btn btn-info btn-sm" ng-click="editar(categoria)">Editar</button>
                            <button type="button" class="btn btn-danger btn-xs" ng-click="excluir(categoria)">Excluir</button>
                        </div>
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
    <!-- Modais de categoria -->
    <!-- Novo cadastro -->
    <div id="modal-novo" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Cadastro de Categorias</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form id="form-categoria">
                        <div ngIf="categoria.id_categoria">
                            <input type="hidden" ng-model="categoria.id_categoria">
                        </div>
                        <div class="form-group">
                            <label for="nome">Nome da Categoria:</label>
                            <input type="text" class="form-control" ng-model="categoria.no_categoria" required>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <button type="button" ng-click="cadastro(categoria)" class="btn btn-success">Cadastrar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Alert Mensagem -->
    <div id="modal-alert" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" ng-click="load()">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="alert {{classe}} alert-dismissible">
                        <strong>{{alerta}}</strong> {{mensagem}}.
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- ./Fim do controller -->