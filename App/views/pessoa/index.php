
<div class="row" ng-controller="pessoaCtrl">
    <div class="col-md-6">
        <table class="table table-hover" id="table-pessoa">
            <thead>
                <tr>
                    <th class="text-center" width="30">ID</th>
                    <th>Nome</th>
                    <th>Email</th>
                    <th class="text-center">
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-novo">
                            Novo
                        </button>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="pessoa in pessoas">
                    <td class="text-center"> {{pessoa.id_pessoa}} </td>
                    <td> {{pessoa.no_nome_completo}} </td>
                    <td> {{pessoa.dc_email}} </td>
                    <td class="text-center"> 
                        <div class="btn-group">
                            <button type="button" class="btn btn-info btn-sm" ng-click="editar(pessoa)">Editar</button>
                            <button type="button" class="btn btn-danger btn-xs" ng-click="excluir(pessoa)">Excluir</button>
                        </div>
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
    <!-- Modais de Pessoa -->
    <!-- Novo cadastro -->
    <div id="modal-novo" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4>Cadastro de Pessoa</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form id="form-pessoa">
                        <div ngIf="pessoa.id_pessoa">
                            <input type="hidden" ng-model="pessoa.id_pessoa">
                        </div>
                        <div class="form-group">
                            <label for="nome">Nome Completo:</label>
                            <input type="text" class="form-control" ng-model="pessoa.no_nome_completo">
                        </div>
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="email" class="form-control" ng-model="pessoa.dc_email">
                        </div>
                        <div class="form-group">
                            <label for="pwd">Senha:</label>
                            <input type="password" class="form-control" ng-model="senha">
                        </div>
                        <div class="form-group">
                            <label for="rpwd">Repetir Senha:</label>
                            <input type="password" class="form-control" ng-model="rsenha">
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <button type="button" ng-click="cadastro(pessoa)" class="btn btn-success">Cadastrar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
    <!-- Modal Alert Mensagem -->
    <div id="modal-alert" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" ng-click="load()">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="alert {{classe}} alert-dismissible">
                        <strong>{{alerta}}</strong> {{mensagem}}.
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
<!-- ./Fim do controller -->