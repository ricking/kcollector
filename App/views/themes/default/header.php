<!DOCTYPE html>
<html lang="pt-br" >
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo isset($this->PageTitle) ? $this->PageTitle : DEFAULT_PAGE_TITLE; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <base href="<?= BASE_URL ?>"/>
        <!-- PortalWeb Conexões - www.portalwebconexoes.com.br  -->
        <meta name="description" content="<?php echo DEFAULT_PAGE_DESCRIPTION; ?>">
        <meta name="keywords" content="<?php echo DEFAULT_PAGE_KEYWORDS; ?>">
        <meta name="author" content="Ricardo Oliveira {PortalWeb Conexões - www.portalwebconexoes.com.br}" />
        <meta name="copyright" content="Copyright &copy; <?php echo ANO; ?>" />
        <meta name="distribution" content="global" />
        <meta name="robots" content="all" />
        <!-- Favicon/Icon -->
        <link rel="shortcut icon" href="img/favicon.ico?v=<?php echo md5_file('img/favicon.ico') ?>" type="image/x-icon" />
        <link rel="icon" href="img/favicon.ico?v=<?php echo md5_file('img/favicon.ico') ?>" type="image/x-icon">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        <!-- Angular -->
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.2/angular.min.js"></script>
        <!-- Meus arquivos css -->
        <?php
        if (isset($this->css) && is_array($this->css)) {
            foreach ($this->css as $css) {
                echo '<link href="' . $css . '" rel="stylesheet"/>' . PHP_EOL;
            }
        }
        ?>
    </head>
    <body>
        <div class="container" ng-app="kApp">

            ​<nav class="navbar navbar-expand-sm bg-light">
                <a class="navbar-brand" href="#">
                    <img src="img/logo.png" alt="Logo" width="100">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div id="navbarNavDropdown" class="navbar-collapse collapse">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="pessoa">Pessoas</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="produto">Produtos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="categoria">Categorias</a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <?php if (!empty($this->getUser('no_nome'))) { ?>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?php echo $this->getUser('no_nome_completo'); ?>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="login/logof">Sair</a>
                                </div>
                            </li>
                        <?php } else { ?>
                            <li class="nav-item">
                                <a class="nav-link" href="login">Login</a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </nav>