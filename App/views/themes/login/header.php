
<!DOCTYPE html>
<html lang="pt-br">
    <head lang="pt-br">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta property="og:site_name" content=""/>
        <meta property="og:title" content=""/>
        <title><?php echo isset($this->PageTitle) ? $this->PageTitle : DEFAULT_PAGE_TITLE; ?></title>
        <base href="<?= BASE_URL ?>"/>

        <link rel="shortcut icon" href="img/favicon.ico?v=<?php echo md5_file('img/favicon.ico') ?>" type="image/x-icon" />
        <link rel="icon" href="img/favicon.ico?v=<?php echo md5_file('img/favicon.ico') ?>" type="image/x-icon">

        <!-- App css -->
        <!-- Bootstrap core CSS -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Perfect -->
        <link href="css/app.min.css" rel="stylesheet">
        <?php
        if (isset($this->css) && is_array($this->css)) {
            foreach ($this->css as $css) {
                echo '<link href="' . $css . '" rel="stylesheet"/>' . PHP_EOL;
            }
        }
        ?>
        <script src="js/jquery-1.10.2.min.js"></script>
    </head>
    <body class="bg-accpunt-pages">

