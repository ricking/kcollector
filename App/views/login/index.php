<div class="login-wrapper">
    <div class="text-center">
        <h2 class="fadeInUp animation-delay8" style="font-weight:bold">
            <span class="text-success">K</span><span style="color:#ccc; text-shadow:0 1px #fff">Collector</span>
        </h2>
    </div>
    <div class="login-widget animation-delay1">	
        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <div class="pull-left">
                    <i class="fa fa-lock fa-lg"></i> Login
                </div>
            </div>
            <div class="panel-body">
                <?php echo!empty($_GET['erro']) ? '<div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                                        Usuário ou senha não encontrado.
						</div>' : '' ?>
                <?php echo!empty($_GET['send']) ? '<div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                                        Um e-mail contendo informações para recuperação de senha
                                                        foi enviado. Verifique a caixa de entrada
                                                        e lixo eletrônico de seu e-mail.
						</div>' : '' ?>
                <form class="form-login" action="login/loginDo" method="post">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="login" placeholder="user@email.com" class="form-control input-sm bounceIn animation-delay2" >
                    </div>
                    <div class="form-group">
                        <label>Senha</label>
                        <input type="password" name="senha" placeholder="*****" class="form-control input-sm bounceIn animation-delay4">
                    </div>
                    <div class="seperator"></div>
                    <hr/>
                    <button type="submit" class="btn btn-success btn-sm bounceIn animation-delay5 pull-right" name="salvar" value="1"><i class="fa fa-sign-in"></i> Entrar</button>
                </form>
            </div>
        </div><!-- /panel -->
    </div><!-- /login-widget -->
</div><!-- /login-wrapper -->








