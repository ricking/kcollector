<?php

namespace App\controllers;

use Framework\core\Controller;

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");

class ProdutoController extends Controller {

    function __construct() {
        parent::__construct();
        $this->view->setActive('produto');
        $this->view->setTheme('default');
    }

    function index() {
        $this->view->setJS(array('index', 'shared/maskMoney'));
        $this->view->render('index');
    }

    function add() {
        $form = file_get_contents("php://input");
        $dados = json_decode($form);
        $pro = $this->loadModel('produto');
        if (!empty($dados->produto)) {
            $pro->setId_produto($dados->produto);
        }
        $pro->setId_categoria($dados->categoria);
        $pro->setDc_produto($dados->nome);
        $valor = $dados->valor;
        $pro->setNm_valor(str_replace(',', '.', $valor));
        $pro->setNm_estoque($dados->estoque);
        $id = $pro->set();
        if ($id) {
            echo json_encode(array('success' => true, 'message' => 'Dados cadastrados com sucesso!', 'id' => $id));
        } else {
            echo json_encode(array('success' => false, 'message' => 'Erro ao efetuar cadastro!'));
        }
    }

    function del($id) {
        if ($id) {
            $pro = $this->loadModel('produto');
            $pro->setId_produto($id);
            if ($pro->delete()) {
                echo 'ok';
            }
        }
    }

    //Api REST
    function get() {
        $pro = $this->loadModel('produto');
        $produtos = $pro->get();
        $response = array();
        foreach ($produtos as $res) {
            $produtos = array();
            $produtos["id_produto"] = $res['id_produto'];
            $produtos["dc_produto"] = $res['dc_produto'];
            $produtos["nm_valor"] = $res['nm_valor'];
            $produtos["nm_estoque"] = $res['nm_estoque'];
            $produtos["id_categoria"] = $res['id_categoria'];
            $produtos["no_categoria"] = $res['no_categoria'];
            array_push($response, $produtos);
        }
        echo json_encode($response);
    }

}
