<?php

namespace App\controllers;

use Framework\core\Controller;

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");

class CategoriaController extends Controller {

    public function __construct() {
        parent::__construct();
        $this->view->setActive('categoria');
        $this->view->setTheme('default');
    }

    public function index() {
        $this->view->setJS(array('index'));
        $this->view->render('index');
    }

    function add() {
        $form = file_get_contents("php://input");
        $dados = json_decode($form);
        $cat = $this->loadModel('categoria');
        if (!empty($dados->categoria)) {
            $cat->setId_categoria($dados->categoria);
        }
        $cat->setNo_categoria($dados->nome);

        $id = $cat->set();
        if ($id) {
            echo json_encode(array('success' => true, 'message' => 'Categoria cadastrada com sucesso!', 'id' => $id));
        } else {
            echo json_encode(array('success' => false, 'message' => 'Erro ao efetuar cadastro!'));
        }
    }

    function del($id) {
        if ($id) {
            $cat = $this->loadModel('categoria');
            $cat->setId_categoria($id);
            if ($cat->delete()) {
                echo 'ok';
            }
        }
    }

    //Api REST
    function get() {
        $cat = $this->loadModel('categoria');
        $categorias = $cat->get();
        $response = array();
        foreach ($categorias as $res) {
            $categorias = array();
            $categorias["id_categoria"] = $res['id_categoria'];
            $categorias["no_categoria"] = $res['no_categoria'];
            array_push($response, $categorias);
        }
        echo json_encode($response);
    }

}
