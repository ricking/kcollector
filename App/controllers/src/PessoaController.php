<?php

namespace App\controllers;

use Framework\core\Controller;

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");

class PessoaController extends Controller {

    public function __construct() {
        parent::__construct();
        //$this->restricted();{caso queira dizer que a página requer um login para funcionar}
        $this->view->setActive('pessoa');
        $this->view->setTheme('default');
    }

    public function index() {
        $this->view->setJS(array('index'));
        $this->view->render('index');
    }

    function add() {
        $form = file_get_contents("php://input");
        $dados = json_decode($form);
        if($dados->senha != $dados->rsenha){
            echo json_encode(array('success' => false, 'message' => 'Senhas não coincidem!'));
            exit;
        }
        $pes = $this->loadModel('pessoa');
        if(!empty($dados->pessoa)){
           $pes->setId_pessoa($dados->pessoa); 
        }
        $pes->setNo_nome_completo($dados->nome);
        $pes->setDc_email($dados->email);
        $pes->setDc_senha(\App\libs\Util::isMD5($dados->senha));
        $pes->setId_pessoa_tipo(1);
        //{por enquanto será definico com 0}            
        //$pes->setId_usuario_bloqueado($this->getPostInt('bloqueado') ? 1 : NULL);
        $id = $pes->set();
        if ($id) {
            echo json_encode(array('success' => true, 'message' => 'Dados cadastrados com sucesso!', 'id' => $id));
        } else {
            echo json_encode(array('success' => false, 'message' => 'Erro ao efetuar cadastro!'));
        }
    }

    function del($id) {
        if ($id) {
            $pes = $this->loadModel('pessoa');
            $pes->setId_pessoa($id);
            if ($pes->delete()) {
                echo 'ok';
            }
        }
    }

    //Api REST
    function get() {
        $pes = $this->loadModel('pessoa');
        $pessoas = $pes->get();
        $response = array();
        foreach ($pessoas as $res) {
            $pessoas = array();
            $pessoas["id_pessoa"] = $res['id_pessoa'];
            $pessoas["no_nome_completo"] = $res['no_nome_completo'];
            $pessoas["dc_email"] = $res['dc_email'];
            array_push($response, $pessoas);
        }
        echo json_encode($response);
        
    }

}
