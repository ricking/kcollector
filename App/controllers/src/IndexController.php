<?php
namespace App\controllers;
use Framework\core\Controller;

class IndexController extends Controller {

    function __construct() {
        parent::__construct();
        $this->view->setActive('index');
    }
    
    function index(){
        $this->view->setCSS(array(''));
        $this->view->setJS(array(''));
        $this->view->render('index');
        //$this->view->renderAjax('construcao');
    }
}
