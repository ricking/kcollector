<?php

namespace App\models;

use Framework\core\Model;

class ProdutoModel extends Model {

    private $id_produto,
            $id_categoria,
            $dc_produto,
            $nm_valor,
            $nm_estoque;

    function __construct() {
        parent::__construct();
    }

    function getId_produto() {
        return $this->id_produto;
    }

    function getId_categoria() {
        return $this->id_categoria;
    }

    function getDc_produto() {
        return $this->dc_produto;
    }

    function getNm_valor() {
        return $this->nm_valor;
    }

    function getNm_estoque() {
        return $this->nm_estoque;
    }

    function setId_produto($id_produto) {
        $this->id_produto = $this->filterVarInt($id_produto);
    }

    function setId_categoria($id_categoria) {
        $this->id_categoria = $this->filterVarInt($id_categoria);
    }

    function setDc_produto($dc_produto) {
        $this->dc_produto = $this->filterVarString($dc_produto);
    }

    function setNm_valor($nm_valor) {
        $this->nm_valor = $this->filterVarFloat($nm_valor);
    }

    function setNm_estoque($nm_estoque) {
        $this->nm_estoque = $this->filterVarNumber($nm_estoque);
    }

    function set() {
        if ($this->getId_produto()) {
            $sql = "UPDATE produto SET id_categoria = ?,dc_produto = ?,nm_valor = ?,nm_estoque = ?"
                    . " WHERE id_produto = ?;";
            $params = array($this->getId_categoria(), $this->getDc_produto(), $this->getNm_valor(),
                $this->getNm_estoque(), $this->getId_produto());
            $res = $this->query($sql, $params);
            $res->rowCount();
            return $this->getId_produto();
        } else {
            $sql = "INSERT INTO produto (id_categoria,dc_produto,nm_valor,nm_estoque) VALUES (?,?,?,?);";
            $params = array($this->getId_categoria(), $this->getDc_produto(), $this->getNm_valor(),
                $this->getNm_estoque());
            $this->query($sql, $params);
            $this->setId_produto($this->lastInsertId);
            return $this->getId_produto();
        }
    }

    function get() {
        $sql = "SELECT p.id_produto,p.dc_produto,p.nm_valor,p.nm_estoque,c.id_categoria,c.no_categoria FROM produto p "
                . "LEFT JOIN categoria c ON c.id_categoria = p.id_categoria";
        $res = $this->query($sql);
        return $res->fetchAll();
    }

    function seletor() {
        $sql = "SELECT p.dc_produto,p.nm_valor,p.nm_estoque,c.id_categoria,c.no_categoria FROM produto p "
                . "LEFT JOIN categoria c ON c.id_categoria = p.id_categoria WHERE p.id_produto = ?";
        $params = array($this->getId_produto());
        $res = $this->query($sql, $params);
        return $res->fetch();
    }
    
    function seletorCategoria() {
        $sql = "SELECT p.dc_produto,p.nm_valor,p.nm_estoque,c.id_categoria,c.no_categoria FROM produto p "
                . "LEFT JOIN categoria c ON c.id_categoria = p.id_categoria WHERE p.id_categoria = ?";
        $params = array($this->getId_categoria());
        $res = $this->query($sql, $params);
        return $res->fetchAll();
    }

    function delete() {
        $sql = "DELETE FROM produto WHERE id_produto = ?;";
        $params = array($this->getId_produto());
        $res = $this->query($sql, $params);
        return $res->rowCount();
    }

}
