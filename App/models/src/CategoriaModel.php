<?php

namespace App\models;

use Framework\core\Model;

class CategoriaModel extends Model {

    private $id_categoria,
            $no_categoria;

    function __construct() {
        parent::__construct();
    }

    function getId_categoria() {
        return $this->id_categoria;
    }

    function getNo_categoria() {
        return $this->no_categoria;
    }

    function setId_categoria($id_categoria) {
        $this->id_categoria = $this->filterVarInt($id_categoria);
    }

    function setNo_categoria($no_categoria) {
        $this->no_categoria = $this->filterVarString($no_categoria);
    }

    function set() {
        if ($this->getId_categoria()) {
            $sql = "UPDATE categoria SET no_categoria = ? WHERE id_categoria = ?;";
            $params = array($this->getNo_categoria(), $this->getId_categoria());
            $res = $this->query($sql, $params);
            $res->rowCount();
            return $this->getId_categoria();
        } else {
            $sql = "INSERT INTO categoria (no_categoria) VALUES (?);";
            $params = array($this->getNo_categoria());
            $this->query($sql, $params);
            $this->setId_categoria($this->lastInsertId);
            return $this->getId_categoria();
        }
    }

    function get() {
        $sql = "SELECT * FROM categoria";
        $res = $this->query($sql);
        return $res->fetchAll();
    }

    function seletor() {
        $sql = "SELECT no_categoria FROM categoria WHERE id_categoria = ?";
        $params = array($this->getId_categoria());
        $res = $this->query($sql, $params);
        return $res->fetch();
    }

    function delete() {
        $sql = "DELETE FROM categoria WHERE id_categoria = ?;";
        $params = array($this->getId_categoria());
        $res = $this->query($sql, $params);
        return $res->rowCount();
    }

}
