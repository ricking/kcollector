-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 05-Ago-2018 às 18:59
-- Versão do servidor: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kcollector`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` bigint(20) NOT NULL,
  `no_categoria` varchar(50) NOT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=4096 DEFAULT CHARSET=utf8;


--
-- Estrutura da tabela `perfil`
--

CREATE TABLE `perfil` (
  `id_pessoa` bigint(20) NOT NULL,
  `id_status` tinyint(3) UNSIGNED DEFAULT '0',
  `dc_img_perfil` varchar(25) DEFAULT NULL,
  `dc_tema` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `perfil`
--

INSERT INTO `perfil` (`id_pessoa`, `id_status`, `dc_img_perfil`, `dc_tema`) VALUES
(8, 0, NULL, NULL),
(12, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoa`
--

CREATE TABLE `pessoa` (
  `id_pessoa` bigint(20) NOT NULL,
  `id_pessoa_tipo` int(10) UNSIGNED DEFAULT NULL,
  `no_nome_completo` varchar(250) NOT NULL,
  `dc_email` varchar(150) NOT NULL,
  `dc_senha` varchar(250) DEFAULT NULL,
  `id_usuario_bloqueado` tinyint(3) UNSIGNED DEFAULT '0',
  `dt_ultimo_acesso` datetime DEFAULT NULL,
  `qt_tentativa_acesso` int(10) UNSIGNED DEFAULT '0',
  `dc_chave_recuperacao_senha` varchar(150) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pessoa`
--

INSERT INTO `pessoa` (`id_pessoa`, `id_pessoa_tipo`, `no_nome_completo`, `dc_email`, `dc_senha`, `id_usuario_bloqueado`, `dt_ultimo_acesso`, `qt_tentativa_acesso`, `dc_chave_recuperacao_senha`) VALUES
(8, 1, 'Cadastro de teste', 'teste@email.com', '$2a$08$MzIyNTU2MjI5NWI2NWFlMOKU6FPC43iZaCbLk8HxpChU/G5DBJ6J2', 0, NULL, 0, NULL),
(12, 1, 'Marieta', 'marieta@email.com', '$2a$08$MTcwNDkwNzAyOTViNjVhYeqKyF/NsSc4qr8XUibMcQDuGo62PEyWG', 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoa_tipo`
--

CREATE TABLE `pessoa_tipo` (
  `id_pessoa_tipo` int(10) UNSIGNED NOT NULL,
  `no_pessoa_tipo` varchar(50) NOT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=5461 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pessoa_tipo`
--

INSERT INTO `pessoa_tipo` (`id_pessoa_tipo`, `no_pessoa_tipo`) VALUES
(1, 'Administrador'),
(2, 'Usuario'),
(3, 'Cliente');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE `produto` (
  `id_produto` bigint(20) NOT NULL,
  `id_categoria` bigint(20) DEFAULT NULL,
  `dc_produto` varchar(250) NOT NULL,
  `nm_valor` float(10,2) DEFAULT NULL,
  `nm_estoque` int(11) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1365 DEFAULT CHARSET=utf8;



--
-- Indexes for dumped tables
--

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indexes for table `perfil`
--
ALTER TABLE `perfil`
  ADD KEY `perfil_FKIndex1` (`id_pessoa`);

--
-- Indexes for table `pessoa`
--
ALTER TABLE `pessoa`
  ADD PRIMARY KEY (`id_pessoa`),
  ADD KEY `pessoa_FKIndex3` (`id_pessoa_tipo`);

--
-- Indexes for table `pessoa_tipo`
--
ALTER TABLE `pessoa_tipo`
  ADD PRIMARY KEY (`id_pessoa_tipo`);

--
-- Indexes for table `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`id_produto`),
  ADD KEY `produto_FKIndex1` (`id_categoria`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `pessoa`
--
ALTER TABLE `pessoa`
  MODIFY `id_pessoa` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `pessoa_tipo`
--
ALTER TABLE `pessoa_tipo`
  MODIFY `id_pessoa_tipo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `produto`
--
ALTER TABLE `produto`
  MODIFY `id_produto` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `perfil`
--
ALTER TABLE `perfil`
  ADD CONSTRAINT `perfil_ibfk_1` FOREIGN KEY (`id_pessoa`) REFERENCES `pessoa` (`id_pessoa`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `pessoa`
--
ALTER TABLE `pessoa`
  ADD CONSTRAINT `pessoa_ibfk_3` FOREIGN KEY (`id_pessoa_tipo`) REFERENCES `pessoa_tipo` (`id_pessoa_tipo`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Limitadores para a tabela `produto`
--
ALTER TABLE `produto`
  ADD CONSTRAINT `produto_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
