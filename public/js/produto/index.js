
app.controller('produtoCtrl', function ($scope, $http, API_URL) {
    var alert = $("#modal-alert");
    //carregar categorias na view
    $http.get(API_URL + "produto/get").then(function (response) {
        $scope.produtos = response.data;
    });
    //carregar categorias no select
    $http.get(API_URL + "categoria/get").then(function (response) {
        $scope.categorias = response.data;
    });
    //método de cadastro e update
    $scope.cadastro = function (produto) {
        var data = {
            produto: produto.id_produto,
            categoria: produto.id_categoria,
            nome: produto.dc_produto,
            valor: $('#valor').val(),
            estoque: produto.nm_estoque};
        console.log(data);
        $http({
            url: API_URL + 'produto/add',
            method: "POST",
            data: data
        }).then(function (response) {
            if (response.data.success) {
                $scope.alerta = "Ok!";
                $scope.mensagem = response.data.message;
                $scope.classe = "alert-success";
                alert.modal('show');
            } else {
                $scope.alerta = "Erro!";
                $scope.mensagem = response.data.message;
                $scope.classe = "alert-danger";
                alert.modal('show');
            }
        });
    };
    //editar cadastro
    $scope.editar = function (produto) {
        $scope.produto = produto;
        $("#modal-novo").modal('show');
    };
    //método excluir
    $scope.excluir = function (produto) {
        var conf = confirm('Tem certeza que deseja apagar esse registro?');
        if (conf) {
            $http.get(API_URL + "produto/del/" + produto.id_produto).then(function (response) {
                if (response.data === 'ok') {
                    alert.modal('show');
                    $scope.alerta = "Ok!";
                    $scope.mensagem = "Cadastro excluído";
                    $scope.classe = "alert-success";
                    location.reload();
                } else {
                    alert.modal('show');
                    $scope.alerta = "Erro!";
                    $scope.mensagem = "Não foi possível excluir!";
                    $scope.classe = "alert-danger";
                }
            });
        } else {
            return false;
        }
    };
    //load page
    $scope.load = function () {
        location.reload();
    };
});
