app.controller('pessoaCtrl', function ($scope, $http, API_URL) {
    var alert = $("#modal-alert");
    //carregar pessoas na view
    $http.get(API_URL +"pessoa/get").then(function (response) {
        $scope.pessoas = response.data;
    });
    //método de cadastro e update
    $scope.cadastro = function (pessoa) {
        var data = {
            pessoa: pessoa.id_pessoa,
            nome: $scope.pessoa.no_nome_completo,
            email: $scope.pessoa.dc_email,
            senha: $scope.senha,
            rsenha: $scope.rsenha};
        $http({
            url: API_URL + 'pessoa/add',
            method: "POST",
            data: data
        }).then(function (response) {
            if (response.data.success) {
                $scope.alerta = "Ok!";
                $scope.mensagem = response.data.message;
                $scope.classe = "alert-success";
                alert.modal('show');
            } else {
                $scope.alerta = "Erro!";
                $scope.mensagem = response.data.message;
                $scope.classe = "alert-danger";
                alert.modal('show');
            }
        });
    };
    //editar cadastro
    $scope.editar = function (pessoa) {
        $scope.pessoa = pessoa;
        console.log($scope.nome);
        $("#modal-novo").modal('show');
    };
    //método excluir
    $scope.excluir = function (pessoa) {
        var conf = confirm('Tem certeza que deseja apagar esse registro?');
        if (conf) {
            $http.get(API_URL +"pessoa/del/" + pessoa.id_pessoa).then(function (response) {
                if (response.data === 'ok') {
                    //resultado
                    alert.modal('show');
                    $scope.alerta = "Ok!";
                    $scope.mensagem = "Cadastro excluído";
                    $scope.classe = "alert-success";
                    location.reload();
                } else {
                    //resultado
                    alert.modal('show');
                    $scope.alerta = "Erro!";
                    $scope.mensagem = "Não foi possível excluir!";
                    $scope.classe = "alert-danger";
                }
            });
        } else {
            return false;
        }
    };
    //load page
    $scope.load = function () {
        location.reload();
    };
});
