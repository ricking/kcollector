app.controller('categoriaCtrl', function ($scope, $http, API_URL) {
    var alert = $("#modal-alert");
    //carregar categorias na view
    $http.get(API_URL + "categoria/get").then(function (response) {
        $scope.categorias = response.data;
    });
    //método de cadastro e update
    $scope.cadastro = function (categoria) {
        var data = {
            categoria: categoria.id_categoria,
            nome: $scope.categoria.no_categoria};
        $http({
            url: API_URL + 'categoria/add',
            method: "POST",
            data: data
        }).then(function (response) {
            if (response.data.success) {
                $scope.alerta = "Ok!";
                $scope.mensagem = response.data.message;
                $scope.classe = "alert-success";
                alert.modal('show');
            } else {
                $scope.alerta = "Erro!";
                $scope.mensagem = response.data.message;
                $scope.classe = "alert-danger";
                alert.modal('show');
            }
        });
    };
    //editar cadastro
    $scope.editar = function (categoria) {
        $scope.categoria = categoria;
        $("#modal-novo").modal('show');
    };
    //método excluir
    $scope.excluir = function (categoria) {
        var conf = confirm('Tem certeza que deseja apagar esse registro?');
        if (conf) {
            $http.get(API_URL + "categoria/del/" + categoria.id_categoria).then(function (response) {
                if (response.data === 'ok') {
                    alert.modal('show');
                    $scope.alerta = "Ok!";
                    $scope.mensagem = "Cadastro excluído";
                    $scope.classe = "alert-success";
                    location.reload();
                } else {
                    alert.modal('show');
                    $scope.alerta = "Erro!";
                    $scope.mensagem = "Não foi possível excluir!";
                    $scope.classe = "alert-danger";
                }
            });
        } else {
            return false;
        }
    };
    //load page
    $scope.load = function () {
        location.reload();
    };
});
